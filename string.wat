(module $string

	(import "wasi_unstable" "fd_write"
		(func $fd_write (param i32 i32 i32 i32) (result i32))
	)

	(memory 72)
	(export "memory" (memory 0))

	(data (i32.const 16) "Lorem ipsum ") ;;Address 12-28
	(data (i32.const 32) "dolor sit amet\n") ;;Address 28-48

	(func $sprint_i32_u (param $n i32) (param $outstr i32)

		(local $digit i32)
		(local $ptr i32)
		(local $startptr i32)
		(local $outlen i32)

		(set_local $ptr (i32.const 71)) ;;End of temporary space

		;;Write each digit starting with the smallest
		(block 
			(loop 

				(set_local $digit
					(i32.rem_u 
						(get_local $n)
						(i32.const 10)
					)
				)

				(set_local $n 
					(i32.div_u
						(get_local $n)
						(i32.const 10)
					)
				)
				
				;;Write char to temp area
				(i32.store8 (get_local $ptr) 
					(i32.add 
						(get_local $digit)
						(i32.const 0x00000030);;Zero character
					)
				)
			
				;;Move the output pointer back one
				(set_local $ptr (i32.sub (get_local $ptr) (i32.const 1)))

				;;Check if loop is done
				(br_if 1 (i32.eqz (get_local $n)))
				(br_if 1 (i32.lt_u (get_local $ptr) (i32.const 52)));;Prevent out of buffer writes
				(br 0)
			)
		)

		;;Determine output size in temporary space
		(set_local $startptr (get_local $ptr))
		(set_local $outlen 
			(i32.sub (i32.const 72) (get_local $startptr))
		)

		;;Copy from temp area to output string
		(i32.store (get_local $outstr) (get_local $outlen))
		
		(call $memcpy_str
			(get_local $startptr)
			(i32.add 
				(get_local $outstr)
				(i32.const 4)
			)
			(get_local $outlen)
		)
		drop
	)

	(func $print_str (param $ptr i32)

		(local $strptr i32)
		(local $len i32)

		;;The string starts 4 bytes after the ptr2,147,483,647
		(set_local $strptr
			(i32.add 
				(get_local $ptr)
				(i32.const 4)
			)
		)

		;;Get length of string
		(set_local $len 
			(i32.load (get_local $ptr))
		)

		;;Write to memory for fd_write usage
		(i32.store (i32.const 0) (get_local $strptr))
		(i32.store (i32.const 4) (get_local $len))
		
		(call $fd_write
			(i32.const 1) 
			(i32.const 0) 
			(i32.const 1) 
			(i32.const 8) 
		)
		drop
	)

	(func $memcpy_str (param $src i32) (param $dst i32) (param $len i32) (result i32)
		
		(local $i i32)
		(set_local $i (i32.const 0))

		(block 
			(loop 
				;;Copy a single byte to dst
				(i32.store8 (get_local $dst) 
					(i32.load8_u (get_local $src))
				)
				
				;;Increment counters by one
				(set_local $i (i32.add (get_local $i) (i32.const 1)))
				(set_local $src (i32.add (get_local $src) (i32.const 1)))
				(set_local $dst (i32.add (get_local $dst) (i32.const 1)))

				;;Check if loop is done
				(br_if 1 (i32.gt_u (get_local $i) (get_local $len)))
				(br 0)
			)
		)

		get_local $dst
	)

	(func $length_str (param $str i32) (result i32)
		
		(i32.load (get_local $str))
	)

	(func $concat_str (param $str1 i32) (param $str2 i32) (param $outstr i32)

		(local $len i32)
		(local $strptr1 i32)
		(local $strptr2 i32)
		(local $outstrptr i32)

		;;Get combined length of string
		(set_local $len 
			(i32.add 
				(i32.load (get_local $str1))
				(i32.load (get_local $str2))
			)
		)

		(i32.store (get_local $outstr) (get_local $len))

		;;The string buffer starts 4 bytes after the main ptr
		(set_local $strptr1
			(i32.add (get_local $str1) (i32.const 4))
		)
		(set_local $strptr2
			(i32.add (get_local $str2) (i32.const 4))
		)
		(set_local $outstrptr
			(i32.add (get_local $outstr) (i32.const 4))
		)

		;;Copy first string to output
		(call $memcpy_str
			(get_local $strptr1)
			(get_local $outstrptr)
			(i32.load (get_local $str1))
		)
		(set_local $outstrptr) ;;Output ptr is modified

		;;Copy second string to output
		(call $memcpy_str
			(get_local $strptr2)
			(get_local $outstrptr)
			(i32.load (get_local $str2))
		)
		drop
	)

	(func $main (export "_start")
		
		(local $outptr i32)
		(local $len i32)
		(local $outptr2 i32)

		;;Address 0 is the address of string to print
		;;Address 4 is the length of string to print
		;;Address 8 is used for the number of bytes written
		;;Address 52-72 number format temp area

		;;Hard code input string lengths
		(i32.store (i32.const 12) (i32.const 12)) 
		(i32.store (i32.const 28) (i32.const 16))

		;;Plan to write joined strings to free memory
		(set_local $outptr (memory.size))

		;;Join strings
		(call $concat_str
			(i32.const 12)
			(i32.const 28)
			(get_local $outptr)
		)

		(call $print_str
			(get_local $outptr)
		)
		
		(set_local $len (i32.load(get_local $outptr)))
		(set_local $outptr2 (memory.size))

		;;Print length of joined string
		(call $sprint_i32_u
			(get_local $len)
			(get_local $outptr2)
		)

		(call $print_str
			(get_local $outptr2)
		)

	)
)
